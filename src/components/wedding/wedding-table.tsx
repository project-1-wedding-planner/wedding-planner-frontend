import { useHistory } from "react-router";

export default function WeddingTable(props:any)
{
    const weddings = props.weddings;
    const history = useHistory<any>();

    return(<table className="table table-striped table-hover">
        <thead>
            <th>ID</th>
            <th>Date</th>
            <th>Name</th>
            <th>Location</th>
        </thead>
        <tbody>
            {weddings.map((w:any) => 
            <tr key={w.id}>
                <td>{w.id}</td>
                <td>{w.date}</td>
                <td>{w.name}</td>
                <td>{w.location}</td>
                <td><button onClick={() => 
                        {
                            w["email"] = history.location.state["email"];
                            history.push({pathname:"/view-wedding", state: w});
                        }
                        }>Select</button></td>
            </tr>)}
        </tbody>
    </table>)
}

